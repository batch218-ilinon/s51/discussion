import {useState} from 'react';
import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function CourseCard({course}) {

  const {name, description, price} = course;
  const [count, setCount] = useState(0);
  const [seats, setSeats] = useState(30);

  function enroll(){
    if(count < 30){
      setCount(count + 1);
      console.log('Enrolless: ' + count);

      setSeats(seats - 1)
      console.log('Seats: ' + count);
    }

    if(count == 30){
      alert("No more seats")
      document.querySelector('#btn-enroll').setAttribute('disabled', true)
    }

  }

  return (
  <Card>
      <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PhP {price}</Card.Text>
          <Card.Text>Enrolless: {count}</Card.Text>
          <Card.Text>Seats: {seats}</Card.Text>
          <Button id="btn-enroll" className="bg-primary" onClick={enroll}>Enroll</Button>
      </Card.Body>
  </Card>
  )
}

// "prototypes" - are a good way of checking data type of information between components.
CourseCard.propTypes = {
  course: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
  })
}